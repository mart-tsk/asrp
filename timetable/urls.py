from django.conf.urls import include, url
from timetable.views import AppointmentListView, TableAppointment, ChangePassword, AppointmentView
from timetable.views import Table, SpecialistsOnFilial,Organiztion, Organiztions, TableAppointmentNotUser
from timetable.views import Login,Test, Register, Root, Edit, ConfirmData, SetNewPhone, ConfirmCodePhone
from django.views.generic import TemplateView


urlpatterns = [   
    url(r'^$', Root.as_view(), name='timetable_root'),
    url(r'^login/', Login.as_view(), name='timetable_login'),
    url(r'^test/', Test.as_view(), name='test'),
    url(r'^registration/', Register.as_view(), name='timetable_registration'),
    url(r'^edit/', Edit.as_view(), name='timetable_edit'),
    url(r'^setnewphone/', SetNewPhone.as_view(), name='timetable_edit_confirmphone'),
    url(r'^confirm/?$', ConfirmData.as_view(), name='timetable_confirm'),
    url(r'^confirmcode/', ConfirmCodePhone.as_view(), name='timetable_edit_confirmcodephone'),
    url(r'^index/', Root.as_view()),
    url(r'^organizations/$', Organiztions.as_view(), name='timetable_organizations'),
    url(r'^organizations/(?P<organization>.[^\/]+)/$', Organiztion.as_view(), name='timetable_organization'),
    url(r'^organizations/(?P<organization>.[^\/]+)/(?P<filial>.[^\/]+)$', SpecialistsOnFilial.as_view(), name='timetable_filial'),
    url(r'^table/(?P<expert>.[^\/]*)$', Table.as_view(), name='timetable_table'),
    url(r'^chanepassword/$',ChangePassword.as_view(),name='timetable_change_password'),
    url(r'^appointment_create/', TableAppointment.as_view(), name='appointment_create'),
    url(r'^appointment_list/', AppointmentListView.as_view(), name='appointment_list'),
    url(r'^appointment/(?P<appointment_id>.[^\/]*)$', AppointmentView.as_view(), name='appointment'),
    url(r'^appointment_new_user/', TableAppointmentNotUser.as_view(), name='appointment_create_new_user'),
]
