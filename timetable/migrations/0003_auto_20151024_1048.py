# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('timetable', '0002_appointment_appointment_status'),
    ]

    operations = [
        migrations.CreateModel(
            name='FastPhone',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('fast_phone', models.CharField(max_length=15, default='')),
                ('fast_phone_code', models.CharField(max_length=5, default='')),
            ],
        ),
        migrations.AlterField(
            model_name='appointment',
            name='appointment_status',
            field=models.CharField(choices=[('NC', 'Не подтверждена'), ('CF', 'Подтверждена'), ('CL', 'Закрыта')], default='NC', max_length=2),
        ),
    ]
