# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('timetable', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='appointment',
            name='appointment_status',
            field=models.CharField(max_length=2, default='NC', choices=[('NC', 'Не подтверждена'), ('CF', 'Подтверждена'), ('CD', 'Закрыта')]),
        ),
    ]
