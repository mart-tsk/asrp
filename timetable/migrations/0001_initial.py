# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.auth.models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
    ]

    operations = [
        migrations.CreateModel(
            name='Appointment',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('appointment_time', models.CharField(max_length=4, blank=True)),
                ('appointment_date', models.DateField(blank=True)),
            ],
            options={
                'verbose_name': 'Запись на прием',
                'verbose_name_plural': 'Записи на прием',
            },
        ),
        migrations.CreateModel(
            name='Appointment_Answer',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('appointment_desc', models.TextField(default=None, max_length=1000)),
                ('appointment_ansver_img', models.ImageField(upload_to='img', verbose_name='Фотография', blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Запись на прием',
                'verbose_name_plural': 'Записи на прием',
            },
        ),
        migrations.CreateModel(
            name='Appointment_Description',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('appointment_description_text', models.TextField(default=None, max_length=1000)),
                ('appointment_description_img', models.ImageField(upload_to='img', verbose_name='Фотография', blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Запись на прием',
                'verbose_name_plural': 'Записи на прием',
            },
        ),
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('city_name', models.CharField(max_length=30)),
            ],
            options={
                'verbose_name': 'Город',
                'verbose_name_plural': 'Города',
            },
        ),
        migrations.CreateModel(
            name='Client',
            fields=[
                ('user_ptr', models.OneToOneField(to=settings.AUTH_USER_MODEL, primary_key=True, parent_link=True, auto_created=True, serialize=False)),
                ('client_phone', models.CharField(max_length=15)),
                ('client_patronymic', models.CharField(default='', max_length=30)),
                ('client_code_email', models.CharField(default='', max_length=15)),
                ('client_code_phone', models.CharField(default='', max_length=5)),
                ('client_confirm_email', models.BooleanField(default=False)),
                ('client_confirm_phone', models.BooleanField(default=False)),
            ],
            options={
                'verbose_name': 'user',
                'abstract': False,
                'verbose_name_plural': 'users',
            },
            bases=('auth.user',),
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='Expert',
            fields=[
                ('user_ptr', models.OneToOneField(to=settings.AUTH_USER_MODEL, primary_key=True, parent_link=True, auto_created=True, serialize=False)),
                ('expert_patronymic', models.CharField(default='', max_length=30)),
                ('expert_description', models.CharField(default='', max_length=2000)),
                ('expert_image', models.ImageField(upload_to='expert', default=None)),
            ],
            options={
                'verbose_name': 'Специалист',
                'verbose_name_plural': 'Специалисты',
            },
            bases=('auth.user',),
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='Filial',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('filial_name', models.CharField(max_length=100)),
                ('filial_street', models.CharField(default='', max_length=50)),
                ('filial_house', models.CharField(default='', max_length=50)),
                ('filial_city', models.ForeignKey(to='timetable.City')),
            ],
        ),
        migrations.CreateModel(
            name='Organization',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('organization_name', models.CharField(max_length=100)),
                ('organization_logo', models.ImageField(upload_to='logos', default=None)),
            ],
            options={
                'verbose_name': 'Организация',
                'verbose_name_plural': 'Организации',
            },
        ),
        migrations.CreateModel(
            name='Speciality',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('speciality_name', models.CharField(max_length=50)),
            ],
            options={
                'verbose_name': 'Специальность',
                'verbose_name_plural': 'Специальности',
            },
        ),
        migrations.CreateModel(
            name='Timetable',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('timetable_start', models.IntegerField(default=None)),
                ('timetable_end', models.IntegerField(default=None)),
                ('timetable_day', models.CharField(default=None, max_length=20, choices=[('Monday', 'Понедельник'), ('Tuesday', 'Вторник'), ('Wednesday', 'Среда'), ('Thursday', 'Четверг'), ('Friday', 'Пятница'), ('Saturday', 'Суббота'), ('Sunday', 'Воскресенье')])),
                ('timetable_expert', models.ForeignKey(to='timetable.Expert', default=None)),
            ],
            options={
                'verbose_name': 'Расписание',
                'verbose_name_plural': 'Расписания',
            },
        ),
        migrations.CreateModel(
            name='Type_Work',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('type_work', models.CharField(default=None, max_length=30)),
            ],
            options={
                'verbose_name': 'Вид приема',
                'verbose_name_plural': 'Виды приемов',
            },
        ),
        migrations.AddField(
            model_name='timetable',
            name='timetable_type',
            field=models.ForeignKey(to='timetable.Type_Work', default=None),
        ),
        migrations.AddField(
            model_name='filial',
            name='filial_organization',
            field=models.ForeignKey(to='timetable.Organization'),
        ),
        migrations.AddField(
            model_name='expert',
            name='expert_filial',
            field=models.ForeignKey(to='timetable.Filial'),
        ),
        migrations.AddField(
            model_name='expert',
            name='expert_speciality',
            field=models.ForeignKey(to='timetable.Speciality'),
        ),
        migrations.AddField(
            model_name='appointment',
            name='appointment_answer',
            field=models.ForeignKey(to='timetable.Appointment_Answer', blank=True, null=True),
        ),
        migrations.AddField(
            model_name='appointment',
            name='appointment_client',
            field=models.ForeignKey(to='timetable.Client', blank=True),
        ),
        migrations.AddField(
            model_name='appointment',
            name='appointment_description',
            field=models.ForeignKey(to='timetable.Appointment_Description', blank=True, null=True),
        ),
        migrations.AddField(
            model_name='appointment',
            name='appointment_expert',
            field=models.ForeignKey(to='timetable.Expert', blank=True),
        ),
        migrations.AddField(
            model_name='appointment',
            name='appointment_type_work',
            field=models.ForeignKey(to='timetable.Type_Work', blank=True),
        ),
    ]
