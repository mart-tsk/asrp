# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('admin', '0001_initial'),
        ('timetable', '0003_auto_20151024_1048'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='expert',
            name='expert_filial',
        ),
        migrations.RemoveField(
            model_name='expert',
            name='expert_speciality',
        ),
        migrations.RemoveField(
            model_name='expert',
            name='user_ptr',
        ),
        migrations.AlterModelOptions(
            name='filial',
            options={'verbose_name_plural': 'Отделения', 'verbose_name': 'Отделение'},
        ),
        migrations.AlterField(
            model_name='appointment',
            name='appointment_expert',
            field=models.ForeignKey(blank=True, to='expertroom.Expert'),
        ),
        migrations.AlterField(
            model_name='timetable',
            name='timetable_expert',
            field=models.ForeignKey(to='expertroom.Expert', default=None),
        ),
        migrations.DeleteModel(
            name='Expert',
        ),
    ]
