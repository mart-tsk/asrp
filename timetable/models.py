from django.db import models
from django.contrib.auth.models import User, UserManager
from tools.string import GenerateString as GS
from django.core.mail import EmailMultiAlternatives
from tools import smsc_api
from django.conf import settings
from tools.url import getUrl
import datetime
from django.views.generic.list import ListView
from django.core.exceptions import ValidationError
from django.core.validators import validate_email

class Organization(models.Model):
	organization_name = models.CharField(max_length=100)
	organization_logo = models.ImageField(upload_to='logos',default=None)

	def __str__(self):
		return self.organization_name
	class Meta:
		verbose_name = 'Организация'
		verbose_name_plural = 'Организации'
		
class City(models.Model):
	city_name = models.CharField(max_length=30)
	def __str__(self):
		return self.city_name	
	class Meta:
		verbose_name = 'Город'
		verbose_name_plural = 'Города'

class Filial(models.Model):
	filial_name = models.CharField(max_length=100)
	filial_organization = models.ForeignKey('Organization')
	filial_city = models.ForeignKey('City')
	filial_street = models.CharField(max_length=50,default="")
	filial_house = models.CharField(max_length=50,default="")
	def __str__(self):
		return self.filial_name
	class Meta:
		verbose_name = 'Отделение'
		verbose_name_plural = 'Отделения'

class Speciality(models.Model):
	speciality_name = models.CharField(max_length=50)
	def __str__(self):
		return self.speciality_name
	class Meta:
		verbose_name = 'Специальность'
		verbose_name_plural = 'Специальности'

class Client(User):
	client_phone = models.CharField(max_length=15)
	client_patronymic = models.CharField(max_length=30, default="")
	client_code_email = models.CharField(max_length=15, default="")
	client_code_phone = models.CharField(max_length=5, default="")
	client_confirm_email = models.BooleanField(default=False)
	client_confirm_phone = models.BooleanField(default=False)

	objects = UserManager()
	def set_email(self, sEmail):
		try:
			Client.objects.get(email=sEmail)
			return False
		except:
			self.email = sEmail
			self.client_confirm_email = False
			self.save()
			self.send_confirm_email()
			return True

	@property
	def full_name(self):
		if not hasattr(self, '_full_name'):
			self._full_name = self.last_name + " " + self.first_name + " " + self.client_patronymic
		return self._full_name


	
	def check_phone(self, cPhone):
		try:
			Client.objects.get(client_phone=cPhone)
			return True
		except:
			return False

	def check_email(email):
		try:
			Client.objects.get(email=email)
			return False
		except:
			try:
				validate_email(email)
			except ValidationError as e:
				return False
			return True

	def check_username(username):
		try:
			Client.objects.get(username=username)
			return False
		except:
			return True

	def set_new_phone(self,sPhone):
		#need some IFes
		self.client_phone = '7' + sPhone
		self.client_confirm_phone = False
		self.save()
		self.send_confirm_phone()


	def set_code_email(self):
		self.client_code_email = GS(15)
		self.save()
		return self.client_code_email

	def set_code_phone(self):
		self.client_code_phone = GS(5)
		self.save()
		return self.client_code_phone

	def generate_password(self, count):
		passw = GS(5)
		self.set_password(passw)
		if self.client_confirm_phone:
			self.send_sms("Ваш пароль для доступа в систему: " +passw)
		self.send_mail("Ваш пароль для доступа в систему: " +passw)
		return passw

	def send_confirm_email(self):
		self.set_code_email()		
		subject, from_email, to = 'Подтверждение регистрации', 'ruka2paka@yandex.ru', self.email
		text_content = 'This is an important message.'
		html_content = '<p>Ваша ссылка для подтверждения: <a href="http://127.0.0.1:8000' + getUrl('timetable_confirm',username=self.username,tpl="email",code=self.client_code_email) + '">нажмите для подтверждения</a> </p>'
		msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
		msg.attach_alternative(html_content, "text/html")
		msg.send()

	def send_confirm_phone(self):
		print (self.set_code_phone())
		smsc = smsc_api.SMSC()
		smsc.send_sms(self.client_phone, "Ваш код подтверждения: " + self.client_code_phone)

	def send_sms(self,text):
		print (text)
		smsc = smsc_api.SMSC()
		smsc.send_sms(self.client_phone, text)

	def send_mail(self,text):
		subject, from_email, to = 'Подтверждение регистрации', 'ruka2paka@yandex.ru', self.email
		text_content = 'This is an important message.'
		html_content =  text  
		msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
		msg.attach_alternative(html_content, "text/html")
		msg.send()

	def confirm_email(self, code):
		if code:
			if self.client_code_email == code:
				self.client_confirm_email = True
				self.client_code_email = ''
				self.save()
				return 0
			else:
				return 1
		else:
			return 1

	def confirm_phone(self, code):
		if code:
			if self.client_code_phone == code:
				self.client_confirm_phone = True
				self.client_code_phone = ""
				self.save()
				return 0
			else:
				return 1
		else:
			return 1

	def is_confirmed(self):
		if not self.client_confirm_phone or not self.client_confirm_email:
			return False
		else:
			return True

	def __str__(self):
		return (self.username + ": " + self.email + ": " + self.client_phone)

class Type_Work(models.Model):
	type_work = models.CharField(max_length=30, default=None)
	def __str__(self):
		return self.type_work
	class Meta:
		verbose_name = 'Вид приема'
		verbose_name_plural = 'Виды приемов'

class Timetable(models.Model):
	timetable_start = models.IntegerField(default=None)
	timetable_end = models.IntegerField(default=None)
	timetable_day_choice = (
        ('Monday', 'Понедельник'),
        ('Tuesday', 'Вторник'),
        ('Wednesday', 'Среда'),
        ('Thursday', 'Четверг'),
		('Friday', 'Пятница'),
		('Saturday', 'Суббота'),
		('Sunday', 'Воскресенье')
    )
	timetable_day = models.CharField(max_length=20,choices=timetable_day_choice,default=None)
	timetable_type = models.ForeignKey('Type_Work',default=None)
	timetable_expert = models.ForeignKey('expertroom.Expert',default=None)
	class Meta:
		verbose_name = 'Расписание'
		verbose_name_plural = 'Расписания'

class Appointment(models.Model):
	appointment_expert = models.ForeignKey('expertroom.Expert', blank=True)
	appointment_time = models.CharField(max_length=4, blank=True)
	appointment_date = models.DateField( blank=True)
	appointment_client = models.ForeignKey('Client', blank=True)
	appointment_type_work = models.ForeignKey('Type_Work', blank=True)
	appointment_description = models.ForeignKey('Appointment_Description', blank=True, null=True)
	appointment_answer = models.ForeignKey('Appointment_Answer', blank=True, null=True)
	NOT_CONFIRMED = 'NC'
	CONFIRMED = 'CF'
	CLOSED = 'CL'
	APPOINTMENT_STATUS_CHOICE = (
		(NOT_CONFIRMED, 'Не подтверждена'),
		(CONFIRMED, 'Подтверждена'),
		(CLOSED, 'Закрыта'),
	)
	appointment_status = models.CharField(max_length = 2, choices=APPOINTMENT_STATUS_CHOICE, default=NOT_CONFIRMED)

	def confirm(self):
		self.appointment_status = self.CONFIRMED
		self.send_info()

	def close(self):
		self.appointment_status = 'CL'

	def send_info(self):
		text = "Вы записаны " + str(self.appointment_date) + " на " + self.appointment_time + " к " + self.appointment_expert.expert_full_name()+". В " + self.appointment_expert.expert_filial.filial_organization.organization_name + " \""+ self.appointment_expert.expert_filial.filial_name+"\""
		smsc = smsc_api.SMSC()
		smsc.send_sms(self.appointment_client.client_phone, text)

	def set_date(self, date):
		self.appointment_date = datetime.datetime.strptime(date, "%Y-%m-%d").date()

	
	def appointment_client_username(self):
		return self.appointment_client.username

	def appointment_expert_type_work(self):
		return self.appointment_type_work.type_work

	def appointment_expert_first_name(self):
		return self.appointment_expert.first_name

	def appointment_expert_last_name(self):
		return self.appointment_expert.last_name

	def appointment_expert_organization(self):
		return self.appointment_expert.expert_filial.filial_organization.organization_name

	def appointment_expert_filial(self):
		return self.appointment_expert.expert_filial.filial_name

	class Meta:
		verbose_name = 'Запись на прием'
		verbose_name_plural = 'Записи на прием'

class Appointment_Description(models.Model):
	appointment_description_text = models.TextField(max_length=1000, default=None)
	appointment_description_img = models.ImageField(upload_to='img', blank=True, null=True, verbose_name='Фотография')

	class Meta:
		verbose_name = 'Запись на прием'
		verbose_name_plural = 'Записи на прием'

class Appointment_Answer(models.Model):
	appointment_desc = models.TextField(max_length=1000, default=None)
	appointment_ansver_img = models.ImageField(upload_to='img', blank=True, null=True, verbose_name='Фотография')

	class Meta:
		verbose_name = 'Запись на прием'
		verbose_name_plural = 'Записи на прием'

class FastPhone(models.Model):
	fast_phone = models.CharField(max_length=15, default="")
	fast_phone_code = models.CharField(max_length=5, default="")

	def set_code_phone(self):
		self.fast_phone_code = GS(5)
		self.save()
		return self.fast_phone_code

	def send_confirm_phone(self):
		print (self.set_code_phone())
		smsc = smsc_api.SMSC()
		smsc.send_sms(self.fast_phone, "Ваш код подтверждения: " + self.fast_phone_code)

	def confirm_phone(self, code):
		if self.fast_phone_code == code:
			return True
		else:
			return False

	def check_phone(cPhone):
		try:
			Client.objects.get(client_phone=cPhone)
			return False
		except:
			return True

