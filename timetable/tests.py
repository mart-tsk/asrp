from django.test import TestCase
from .models import Client
# Create your tests here.

class TestSetEmail(TestCase):
	def test_email_set_in_base(self):
		email = 'first@mail.ru'
		user = Client.objects.create(
										username='user_login',
										first_name='user_name',
										last_name='user_last_name',
										email='email',
										client_phone='user_phone',
										client_patronymic='user_patronymic'
									)
		user.set_email(email)
		self.assertEqual(user.email,'first@mail.ru')

class TestSetEmail2(TestCase):
	def test_email_set_repeat_in_base(self):
		email = 'first@mail.ru'
		user = Client.objects.create(
										username='user_login',
										first_name='user_name',
										last_name='user_last_name',
										email='email',
										client_phone='user_phone',
										client_patronymic='user_patronymic'
									)
		user.set_email('first@mail.ru')
		self.assertEqual(user.set_email(email),False)
