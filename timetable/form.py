from django import forms
from django.forms import widgets
from timetable.models import Speciality, Client, City
from expertroom.models import Expert

class LoginForm(forms.Form):
	login_form_username = forms.CharField(label='User Name', max_length=30, widget=widgets.TextInput(attrs={'class': 'form-control  input-lg'}))
	login_form_password = forms.CharField(label='Password', widget=widgets.PasswordInput(attrs={'class': 'form-control  input-lg'}))

class RegisterForm(forms.Form):
	register_form_username = forms.CharField(label='User Name', max_length=30,min_length=4, widget=widgets.TextInput(attrs={'class': 'form-control  input-lg','placeholder': 'UserLogin'}))
	register_form_name = forms.CharField(label='Name', max_length=30,min_length=2, widget=widgets.TextInput(attrs={'class': 'form-control  input-lg', 'placeholder': 'Василий'}))
	register_form_last_name = forms.CharField(label='Last Name',min_length=2, max_length=30, widget=widgets.TextInput(attrs={'class': 'form-control  input-lg', 'placeholder': 'Теркин'}))
	register_form_patronymic = forms.CharField(label='Patronymic Name',min_length=2, max_length=30, widget=widgets.TextInput(attrs={'class': 'form-control  input-lg', 'placeholder': 'Иванович'}))
	register_form_email = forms.CharField(label='Email', max_length=30,min_length=5, widget=widgets.TextInput(attrs={'class': 'form-control  input-lg','placeholder': 'email@example.ru'}))
	register_form_phone = forms.CharField(label='Phone', max_length=10,min_length=10, widget=widgets.TextInput(attrs={'class': 'form-control  input-lg', 'id':'telnum', 'placeholder': '9991113233'}))
	register_form_password = forms.CharField(label='Password',min_length=4, widget=widgets.PasswordInput(attrs={'class': 'form-control  input-lg',}))
	register_form_password_confirm = forms.CharField(label='Password Confirm',min_length=4, widget=widgets.PasswordInput(attrs={'class': 'form-control  input-lg'}))

class EditForm(forms.Form):
	edit_form_name = forms.CharField(label='Имя', max_length=30, widget=widgets.TextInput(attrs={'class': 'form-control  input-lg'}))
	edit_form_last_name = forms.CharField(label='Фамилия', max_length=30, widget=widgets.TextInput(attrs={'class': 'form-control  input-lg'}))
	edit_form_patronymic = forms.CharField(label='Отчество', max_length=30, widget=widgets.TextInput(attrs={'class': 'form-control  input-lg'}))
	edit_form_email = forms.CharField(label='Email', max_length=30, widget=widgets.TextInput(attrs={'type':'Email', 'class': 'form-control  input-lg'}))
	edit_form_phone = forms.CharField(label='Телефон', min_length=10, max_length=10, widget=widgets.TextInput(attrs={'class': 'form-control  input-lg', 'readonly': '', 'id':'telnum'}))
	
class PhoneForm(forms.Form):
	phone_form_phone = forms.CharField(label='Новый номер', max_length=10, widget=widgets.TextInput(attrs={'required':'','Type':'tel','class': 'form-control  input-lg', 'id':'telnum'}))

class ConfirmCodePhoneForm(forms.Form):
	confirm_code_phone_form_code = forms.CharField(label='Код подтверждения', max_length=5, widget=widgets.TextInput(attrs={'required':'','Type':'tel','class': 'form-control  input-lg'}))

class ChooseCityForm(forms.Form):
	choose_city_form_city = forms.ModelChoiceField(label='Город',widget=widgets.Select(attrs={'onchange':'this.form.submit()','class':'form-control','id':'select'}),queryset=City.objects.all())

class ChooseSpecialityForm(forms.Form):
	choose_speciality_form_speciality = forms.ModelChoiceField(label='Специальность',widget=widgets.Select(attrs={'onchange':'this.form.submit()','class':'form-control','id':'select'}),queryset=Speciality.objects.all())
	def __init__(self, *args, **kwargs):
		filial_id = kwargs.pop('filial_id')
		super(ChooseSpecialityForm, self).__init__(*args, **kwargs)
		v = Expert.objects.filter(expert_filial = filial_id).values_list('expert_speciality',flat=True)
		self.fields['choose_speciality_form_speciality'].queryset = Speciality.objects.filter(id__in=list(v))

class ChangePasswordForm(forms.Form):
	change_password_form_old_password = forms.CharField(label='Старый пароль', widget=widgets.PasswordInput(attrs={'required':'','class': 'form-control  input-lg'}))
	change_password_form_new_password = forms.CharField(label='Новый пароль',min_length=4, widget=widgets.PasswordInput(attrs={'required':'','class': 'form-control  input-lg',}))
	change_password_form_new_password_confirm = forms.CharField(label='Потверждение нового пароля',min_length=4, widget=widgets.PasswordInput(attrs={'required':'','class': 'form-control  input-lg'}))

class AppointmentForm(forms.Form):
	appointment_form_text = forms.CharField(label='Описание', widget=forms.Textarea(attrs={'class': 'form-control noresize'}), required=False)
	appointment_form_img = forms.ImageField(label='Изображение', required=False)

class FastRegisterForm(forms.Form):
	fast_register_form_username = forms.CharField(label='User Name', max_length=30,min_length=4, widget=widgets.TextInput(attrs={'class': 'form-control  input-lg','placeholder': 'Login'}))
	fast_register_form_name = forms.CharField(label='Name', max_length=30,min_length=2, widget=widgets.TextInput(attrs={'class': 'form-control  input-lg', 'placeholder': 'Василий'}))
	fast_register_form_last_name = forms.CharField(label='Last Name',min_length=2, max_length=30, widget=widgets.TextInput(attrs={'class': 'form-control  input-lg', 'placeholder': 'Теркин'}))
	fast_register_form_patronymic = forms.CharField(label='Patronymic Name',min_length=2, max_length=30, widget=widgets.TextInput(attrs={'class': 'form-control  input-lg', 'placeholder': 'Иванович'}))
	fast_register_form_email = forms.CharField(label='Email', max_length=30,min_length=5, widget=widgets.TextInput(attrs={'class': 'form-control  input-lg','placeholder': 'email@example.ru'}))