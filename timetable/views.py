from datetime import datetime
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render_to_response, redirect, HttpResponse
from django.core.urlresolvers import reverse
from django.views.generic import View
from tools.csrf import GetFormCSRF
from django.http import Http404
from timetable.form import AppointmentForm, ChangePasswordForm, ChooseSpecialityForm, ChooseCityForm, LoginForm, RegisterForm, EditForm, PhoneForm, ConfirmCodePhoneForm, FastRegisterForm
from timetable.models import FastPhone, Type_Work, Appointment_Description, Appointment, Speciality, City, Filial,  Organization, Client, Timetable
from expertroom.models import Expert
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.forms.models import modelformset_factory
from tools.url import getUrl
from tools.notification import Notification
from tools.day import getDay, Days, curDate
from tools.decorators import client_auth, client_auth
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from tools.auth_be import getUserType
from django.db.models import Q

class Test(View):
	def get(self, request):
		title = "Авторизация"
		return HttpResponse(isinstance(request.user,Client))
		form = LoginForm()
		if request.user.is_authenticated():
			return render_to_response('test.html', {'user': request.user})
		else:
			return request.user.groups

class Login(View):
	def get(self, request):
		title = "Авторизация"
		form = LoginForm()
		notes = []
		if request.GET.get('logout') == "true" and request.user.is_authenticated():
			logout(request)
		if request.GET.get('password') == "changed":
			note = Notification(Notification.enumTypeNotification.Info,"Пароль изменен", "Ваш пароль изменен, для продолжения работы войдите под новым паролем")
			notes.append(note)
		return render_to_response('login.html',
								  {'user': request.user,
								   'title': title,
								   'notes':notes,
								   'form': form,
								   'csrf_token': GetFormCSRF(request)})

	def post(self, request):
		title = "Авторизация"
		form = LoginForm(request.POST)
		result = None
		if form.is_valid():
			user_login = form.cleaned_data['login_form_username']
			user_password = form.cleaned_data['login_form_password']
			user = authenticate(username=user_login, password=user_password)
			if user is not None:
				if user.is_active:
					login(request, user)
					return redirect(reverse('timetable_root'))
				else:
					result = 2
			else:
				result = 1
			return render_to_response('login.html',
									  {'user': request.user,
									   'title': title,
									   'form': form,
									   'csrf_token': GetFormCSRF(request),
									   'result': result
									   })


class Register(View):
	def get(self, request):
		title = "Регистрация"
		form = RegisterForm()
		return render_to_response('register.html',
								  {'user': request.user,
								   'title': title,
								   'form': form,
								   'csrf_token': GetFormCSRF(request)})

	def post(self, request):
		title = "Регистрация"
		form = RegisterForm(request.POST)
		result = -1
		if form.is_valid():
			result = 0
			user_login = form.cleaned_data['register_form_username']
			user_name = form.cleaned_data['register_form_name']
			user_last_name = form.cleaned_data['register_form_last_name']
			user_patronymic = form.cleaned_data['register_form_patronymic']
			user_email = form.cleaned_data['register_form_email']
			user_phone = form.cleaned_data['register_form_phone']
			user_password = form.cleaned_data['register_form_password']
			user_password_confirm = form.cleaned_data['register_form_password_confirm']
			if user_password != user_password_confirm:
				result = 1
			if User.objects.filter(username=user_login):
				result = 2
			if User.objects.filter(email=user_email):
				result = 3
			try:
				validate_email(user_email)
			except ValidationError as e:
				result = 4
			if Client.objects.filter(client_phone=user_phone):
				result = 5

			if result == 0:
				client = Client.objects.create(
					username=user_login,
					first_name=user_name,
					last_name=user_last_name,
					client_patronymic=user_patronymic
				)
				client.set_password(user_password)
				client.save()
				client.set_new_phone(user_phone)
				client.set_email(user_email)
				user = authenticate(username=user_login, password=user_password)
				if user is not None:
					if user.is_active:
						login(request, user)
				return redirect(reverse('timetable_root'))

		return render_to_response('register.html',
								  {
									  'user': request.user,
									  'title': title,
									  'form': form,
									  'csrf_token': GetFormCSRF(request),
									  'result': result
								  })


class Root(View):
	def get(self, request):
		notes = []
		title = "Medical Base"
		if request.user.is_authenticated():
			if request.user.is_superuser:
				return redirect('/admin/')
			if getUserType(request.user) == 'Operator':
				return redirect(reverse('operatorroom_AppointmentListView'))
			if getUserType(request.user) == 'Expert':
				return redirect(reverse('expert_appointment_list'))
			if request.GET.get('email') == 'confirm':
					note = Notification(Notification.enumTypeNotification.Succes,"E-Mail Подтвержден", "Ваш E-Mail подтвержден.")
					notes.append(note)
			if request.user.client.is_confirmed():
				note = Notification(Notification.enumTypeNotification.Warning,"Неверный телефон", "fdg")
				return render_to_response('test.html', {
														'user': request.user,
														'notes':notes,
														'title':title
														})
			else:
				if request.GET.get('email') == 'resend':
					request.user.client.send_confirm_email()
					return redirect(getUrl('timetable_root', email='send'))
				if request.GET.get('email') == 'send':
					note = Notification(Notification.enumTypeNotification.Succes,"Письмо отправлено", "Письмо с ссылкой подтверждения отправлено на вашу почту. Перейдите по ссылке в письме для подтверждения вашей почты.")
					notes.append(note)
				
				return render_to_response('info_confirm.html', {'user': request.user, 'notes':notes, 'title':title})
		else:
			return redirect(reverse('timetable_login'))


class Edit(View):

	@method_decorator(client_auth)
	def dispatch(self, *args, **kwargs):
		return super(Edit, self).dispatch(*args, **kwargs)
	
	def get(self, request):
		notes = []
		title = "Редактирование профиля"
		form = EditForm(initial={
			'edit_form_name': request.user.first_name,
			'edit_form_last_name': request.user.last_name,
			'edit_form_email': request.user.email,
			'edit_form_patronymic': request.user.client_patronymic,
			'edit_form_phone': request.user.client_phone, })
		if request.GET.get('succes') == 'y':
			note = Notification(Notification.enumTypeNotification.Succes,"Изменения сохранены", "")
			notes.append(note)
		return render_to_response('edit.html',
								  {'user': request.user,
								   'title': title,
								   'notes': notes,
								   'form': form,
								   'csrf_token': GetFormCSRF(request)})

	def post(self, request):
		title = "Редактирование профиля"
		form = EditForm(request.POST)
		if form.is_valid():
			print("sda")
			user_first_name = form.cleaned_data['edit_form_name']
			user_last_name = form.cleaned_data['edit_form_last_name']
			user_email = form.cleaned_data['edit_form_email']
			user_patronymic = form.cleaned_data['edit_form_patronymic']
			request.user.first_name = user_first_name
			request.user.last_name = user_last_name
			request.user.client_patronymic = user_patronymic
			if user_email != request.user.email:
				request.user.set_email(user_email)
			request.user.save()
			return redirect(getUrl('timetable_edit', succes='y'))
		return render_to_response('edit.html',
								  {'user': request.user,
								   'title': title,
								   'form': form,
								   'csrf_token': GetFormCSRF(request),
								   })


class SetNewPhone(View):
	def get(self, request):
		current_user = Client.objects.get(id=request.user.id)
		title = "Изменение номера телефона"
		form = PhoneForm()
		return render_to_response('phoneconfirm.html',
								  {'user': request.user,
								   'title': title,
								   'form': form,
								   'error': "Get Set New phone",
								   'csrf_token': GetFormCSRF(request),
								   })

	def post(self, request):
		form = PhoneForm(request.POST)
		current_user = Client.objects.get(id=request.user.id)
		title = "Изменение номера телефона"
		notes = []
		if form.is_valid():
			if current_user.client_phone == form.cleaned_data['phone_form_phone']:
				note = Notification(Notification.enumTypeNotification.Danger,"Повтор номера", "Вы ввели номер, который уже привязан к вашему аккаунту, введите другой, или, для того чтобы оставить этот, просто ввыйдите из этого раздела")
				notes.append(note)				
			if current_user.check_phone(form.cleaned_data['phone_form_phone']):
				note = Notification(Notification.enumTypeNotification.Danger,"Номер уже используется", "Номер который вы ввели уже используется другим пользователем, введите, пожалуйста, свой номер или позвоните в поддержку")
				notes.append(note)
			current_user.set_new_phone(form.cleaned_data['phone_form_phone'])
			return redirect(reverse('timetable_edit_confirmcodephone'))
		else:
			note = Notification(Notification.enumTypeNotification.Danger,"Ошибка данных формы", "Введите валидные данные")
			notes.append(note)
		return render_to_response('phoneconfirm.html',
								  {'user': request.user,
								   'title': title,
								   'form': form,
								   'notes': notes,
								   'csrf_token': GetFormCSRF(request),
								   })


class ConfirmCodePhone(View):
	def get(self, request):
		current_user = Client.objects.get(id=request.user.id)
		title = "Подтверждение номера телефона"
		form = ConfirmCodePhoneForm()
		if (request.GET.get('code')=='resend'):
			current_user.send_confirm_phone()
			return redirect(getUrl('timetable_edit_confirmcodephone', code='send'))
		if (request.GET.get('code')=='send'):
			return render_to_response('codephoneconfirm.html',
								  {'user': request.user,
								   'title': title,
								   'form': form,
								   'error': "CodeResendet",
								   'phone': current_user.client_phone,
								   'csrf_token': GetFormCSRF(request),
								   })
		
		return render_to_response('codephoneconfirm.html',
								  {'user': request.user,
								   'title': title,
								   'form': form,
								   'error': "iT s get",
								   'phone': current_user.client_phone,
								   'csrf_token': GetFormCSRF(request),
								   })

	def post(self, request):
		form = ConfirmCodePhoneForm(request.POST)
		current_user = Client.objects.get(id=request.user.id)
		title = "Подтвеждение номера телефона"
		if form.is_valid():
			if current_user.confirm_phone(form.cleaned_data['confirm_code_phone_form_code']):
				return render_to_response('codephoneconfirm.html',
										  {'user': request.user,
										   'title': title,
										   'form': form,
										   'phone': current_user.client_phone,
										   'error': 'ErrorCode',
										   'csrf_token': GetFormCSRF(request),
										   })
			else:
				return redirect(getUrl('timetable_edit', succes='y'))
		else:
			return render_to_response('codephoneconfirm.html',
									{'user': request.user,
									   'title': title,
									   'form': form,
									   'phone': current_user.client_phone,
									   'error': 'ErrorPost',
									   'csrf_token': GetFormCSRF(request),
									   })


class ConfirmData(View):
	def get(self, request):
		if request.GET.get("username") and request.GET.get("tpl") and request.GET.get("code"):
			try:
				client = Client.objects.get(username=request.GET.get("username"))
				if request.GET.get("tpl") == "email":
					if client.confirm_email(request.GET.get("code")) == 0:
						return redirect(getUrl('timetable_root', email='confirm'))
					else:
						raise Http404("Неверный код")
				elif request.GET.get("tpl") == "phone":
					if client.confirm_phone(request.GET.get("code")) == 0:
						return redirect(reverse('timetable_root'))
					else:
						raise Http404("Неверный код")
				else:
					return redirect(reverse('timetable_root'))
			except Client.DoesNotExist:
				raise Http404("Аккаунт не найден")
		else:
			return redirect(reverse('timetable_root'))

class Organiztions(View):
	def get(self, request):
		title = "Список организаций"
		organizations = Organization.objects.all()
		return render_to_response('organizations.html',
									  {'user': request.user,
									   'title': title,
									   'error': 'нет',
									   'organizations':organizations,
									   })

class Organiztion(View):
	def get(self, request, organization):
		title = "Список филиалов"
		current_organization = Organization.objects.get(organization_name=organization)
		filials = current_organization.filial_set.all()
		form = ChooseCityForm()
		return render_to_response('filials.html',
									  {'user': request.user,
									   'title': title,
									   'error': 'нет',
									   'current_organization':current_organization,
									   'filials':filials,
									   'form':form,
									   'csrf_token': GetFormCSRF(request),
									   })
	def post(self, request, organization):
		title = "Список филиалов"
		current_organization = Organization.objects.get(organization_name=organization)		
		form = ChooseCityForm(request.POST)
		if form.is_valid():
			current_city = form.cleaned_data['choose_city_form_city']
			filials = Filial.objects.filter(filial_organization=current_organization,filial_city=current_city)
			return render_to_response('filials.html',
										  {'user': request.user,
										   'title': title,
										   'error': 'нет',
										   'current_organization':current_organization,
										   'filials':filials,
										   'form':form,
										   'csrf_token': GetFormCSRF(request),
										   })
		else:
			filials = Filial.objects.all()
			return render_to_response('filials.html',
										  {'user': request.user,
										   'title': title,
										   'error': 'нет',
										   'current_organization':current_organization,
										   'filials':filials,
										   'form':form,
										   'csrf_token': GetFormCSRF(request),
										   })

class SpecialistsOnFilial(View):
	def get(self, request, organization,filial):
		title = "Список специалистов"
		current_filial = Filial.objects.get(filial_name=filial)
		form = ChooseSpecialityForm(filial_id=current_filial.id)
		return render_to_response('specs_filial.html',
									  {'user': request.user,
									   'title': title,
									   'error': 'ChooseSpec',
									   'organization':organization,
									   'form':form,
									   'csrf_token': GetFormCSRF(request), 
									   'current_filial':current_filial, 
									   })
	def post(self, request, organization, filial):
		title = "Список специалистов"
		current_filial = Filial.objects.get(filial_name=filial)
		form = ChooseSpecialityForm(request.POST, filial_id=current_filial.id)
		if form.is_valid():
			current_cpec = form.cleaned_data['choose_speciality_form_speciality']
			experts = Expert.objects.filter(expert_filial=current_filial,expert_speciality=current_cpec)
			return render_to_response('specs_filial.html',
										  {'user': request.user,
										   'title': title,
										   'error': 'нет',
										   'organization':organization,
										   'experts':experts,
										   'form':form,
										   'current_filial':current_filial, 
										   'csrf_token': GetFormCSRF(request),
										   })
		else:
			return render_to_response('specs_filial.html',
									  {'user': request.user,
									   'title': title,
									   'error': 'нет',
									   'organization':organization,
									   'form':form,
									   'current_filial':current_filial, 
									   'csrf_token': GetFormCSRF(request),  
									   })

class Table(View):
	def get(self, request, expert):	
		try:	
			expert = Expert.objects.get(id=expert)
			title = "Medical Base - " + expert.expert_full_name()
			table = []
			for x in range(0,7):
				rasp = {
							'date':curDate(x),
							'rasp':[]
						}
				tms = Timetable.objects.filter(
					timetable_expert=expert
					).filter(timetable_day=getDay(curDate(x))).order_by('timetable_start')
				closed_app = Appointment.objects.filter(Q(appointment_date=curDate(x)) & ~Q(appointment_status=Appointment.CLOSED))
				closed_time = []
				for cap in closed_app:
					closed_time.append(cap.appointment_time)
				for tm in tms:
					lines = tm.timetable_end - tm.timetable_start
					for x in range(0,lines+1):
						cache = {
									'name':tm.timetable_type.type_work,
									'table_type_id':tm.timetable_type.id,
									'times':[]
								}
						for j in range(0,4):
							time = 15*j
							if time == 0:
								time = str(tm.timetable_start+x)+":"+str(time)+"0"
							else:
								time = str(tm.timetable_start+x)+":"+str(time)
							time_construct = {
												'time':time,
												'close':False
											 }
							for k in closed_time:
								if k == time_construct['time']:
									time_construct['close'] = True
							cache['times'].append(time_construct)
						rasp['rasp'].append(cache)
				table.append(rasp)
			return render_to_response('table.html',
									{
										'title':title,
										'user': request.user,
										'expert': expert,
										'timetablelist':table,
									   'csrf_token': GetFormCSRF(request),  

									})
		except Expert.DoesNotExist:
			raise Http404("Специалист не найден")

class ChangePassword(View):	
	def get(self, request):
		form = ChangePasswordForm()
		title = "Изменение пароля"
		return render_to_response('changepassword.html',
									  {'user': request.user,
									   'title': title,
									   'error': 'нет',
									   'form':form,
									   'csrf_token': GetFormCSRF(request),  
									   })
	def post(self,request):
		notes = []
		form = ChangePasswordForm(request.POST)
		title = "Изменение пароля"
		if form.is_valid():
			old = form.cleaned_data['change_password_form_old_password']
			new = form.cleaned_data['change_password_form_new_password']
			conf = form.cleaned_data['change_password_form_new_password_confirm']
			if request.user.check_password(old):
				if new==conf:
					request.user.set_password(new)
					request.user.save()
					note = Notification(Notification.enumTypeNotification.Succes,"Пароль изменен", "Пароль был успешно изменен")
					notes.append(note)
					return redirect(getUrl('timetable_login', password='changed'))
				else:
					note = Notification(Notification.enumTypeNotification.Danger,"Пароли не совпадают", "Новый пароль и его подтверждение не совпадают")
					notes.append(note)
			else:
				note = Notification(Notification.enumTypeNotification.Danger,"Указан не верный пароль", "Вы указали невенрый пароль в поле \"Старый пароль\"")
				notes.append(note)
			
		else:
			note = Notification(Notification.enumTypeNotification.Danger,"Ошибка формы", "Ошибка заполнения формы, укажите валидные данные")
			notes.append(note)
		return render_to_response('changepassword.html',
									  {'user': request.user,
									   'title': title,
									   'error': 'нет',
									   'notes': notes,
									   'form':form,
									   'csrf_token': GetFormCSRF(request),  
									   })

class TableAppointment(View):
	@method_decorator(client_auth)
	def dispatch(self, *args, **kwargs):
		return super(TableAppointment, self).dispatch(*args, **kwargs)

	def get(self,request):
		return redirect(reverse('timetable_root'))

	def post(self,request):
		try:
			title = "Оформление заявки"
			form = None
			text = request.POST.get('text')
			expert = Expert.objects.get(id=request.POST.get('expert_id'))
			dates = []
			for x in text.split('/'):
				dates.append({ 'time':x.split("|")[0], 'date':x.split("|")[1], 'table_type':x.split("|")[2]})
			type_work = Type_Work.objects.get(id=dates[0]['table_type'])

			if request.POST.get('typeQuery') == 'zp':
				form = AppointmentForm()
			else:				
				form = AppointmentForm(request.POST, request.FILES)
				if form.is_valid():
					description = None
					
					if form.cleaned_data['appointment_form_text'] or form.cleaned_data['appointment_form_img']:
						description = Appointment_Description()
						description.appointment_description_text = form.cleaned_data['appointment_form_text']
						description.appointment_description_img = form.cleaned_data['appointment_form_img']
						description.save()

					for x in dates:
						appointment = Appointment()
						appointment.appointment_expert = expert
						appointment.appointment_client = request.user.client
						appointment.appointment_type_work = type_work
						if description:
							appointment.appointment_description = description
						appointment.appointment_time = x['time']
						appointment.set_date(x['date'])
						appointment.save()
					return redirect(getUrl('appointment_list',add="appointment"))
						
			return render_to_response('tableappointment.html',
										{
											'user': request.user,
											'title': title,
											'expert':expert,
											'text':text,
											'form':form,
											'dates':dates,
											'type_work':type_work,
											'csrf_token': GetFormCSRF(request),  
										})
		except Expert.DoesNotExist:
			raise Http404("Специалист не найден")
		except Type_Work.DoesNotExist:
			raise Http404("Тип приема не найден")


class AppointmentListView(View):
	@method_decorator(client_auth)
	def dispatch(self, *args, **kwargs):
		return super(AppointmentListView, self).dispatch(*args, **kwargs)

	def get(self, request):
		notes = []
		title = "Список заявок"
		if request.GET.get('add') == "appointment":
			note = Notification(Notification.enumTypeNotification.Succes,"Заявка добавлена", "Ваша заявка добавлена, в скором временни она пройдет проверку оператором.")
			notes.append(note)
		appointment_list = Appointment.objects.filter(appointment_client = request.user.client).order_by('-id')

		return render_to_response('appointment_list.html',
										{
											'user': request.user,
											'title': title,
											'appointment_list':appointment_list,
											'notes':notes,
											'csrf_token': GetFormCSRF(request),  
										})

class AppointmentView(View):
	@method_decorator(client_auth)
	def dispatch(self, *args, **kwargs):
		return super(AppointmentView, self).dispatch(*args, **kwargs)

	def get(self, request, appointment_id):
		notes = []
		title = "Ваша заявка"
		appointment = Appointment.objects.get(id=appointment_id)
		return render_to_response('appointment.html',
										{
											'user': request.user,
											'title': title,
											'appointment':appointment,
											'notes':notes,
											'csrf_token': GetFormCSRF(request),  
										})

class TableAppointmentNotUser(View):
	def get(self,request):
		return redirect(reverse('timetable_root'))

	def post(self,request):
		try:
			title = "Оформление заявки"
			form = None
			phone_fast = None
			trigger = True
			notes = []
			step = 1
			if request.POST.get('step'):
				step = int(request.POST.get('step'))
			text = request.POST.get('text')
			expert = Expert.objects.get(id=request.POST.get('expert_id'))
			dates = []
			for x in text.split('/'):
				dates.append({ 'time':x.split("|")[0], 'date':x.split("|")[1], 'table_type':x.split("|")[2]})
			type_work = Type_Work.objects.get(id=dates[0]['table_type'])

			if step == 1:
				form = PhoneForm(request.POST)
				if form.is_valid():
					phone = "7" + form.cleaned_data['phone_form_phone']
					if FastPhone.check_phone(phone):
						phone_fast = FastPhone(fast_phone=phone)
						phone_fast.save()
						phone_fast.send_confirm_phone()
						phone_fast.save()
						step = 2
					else:
						notes.append(Notification(Notification.enumTypeNotification.Danger,"Телефон занят",
													 "Данный телефон используется. Введите другой номер \
													  телефона или выполните вход в систему."))
			if step == 2:
				form = ConfirmCodePhoneForm(request.POST)
				if form.is_valid():
					code = form.cleaned_data['confirm_code_phone_form_code']
					phone_fast = FastPhone.objects.get(id=request.POST.get('phone_fast_id'))
					if phone_fast.confirm_phone(code):
						step = 3
					else:
						notes.append(Notification(Notification.enumTypeNotification.Danger,"Неверный код подтверждения", "Вы ввели неверный код подтверждения."))

			if step == 3:
				form = FastRegisterForm(request.POST)				
				if form.is_valid():
					username = form.cleaned_data['fast_register_form_username']
					name = form.cleaned_data['fast_register_form_name']
					last_name = form.cleaned_data['fast_register_form_last_name']
					patronymic = last_name = form.cleaned_data['fast_register_form_patronymic']
					email = last_name = form.cleaned_data['fast_register_form_email']
					
					if not Client.check_username(username):
						notes.append(Notification(Notification.enumTypeNotification.Danger,"Логин уже используется", "Придумайте другой логин."))
					elif not Client.check_email(email):
						notes.append(Notification(Notification.enumTypeNotification.Danger,"Емайл уже используется", "Введите другой емайл."))
					else:
						client = Client()
						client.username = username
						client.email = email
						client.first_name = name
						client.last_name = last_name
						client.client_patronymic = patronymic
						phone_fast = FastPhone.objects.get(id=request.POST.get('phone_fast_id'))
						client.client_phone = phone_fast.fast_phone
						client.client_confirm_phone = True
						passw = client.generate_password(5)
						client.save()
						user = authenticate(username=username, password=passw)
						if user is not None:
							if user.is_active:
								login(request, user)
						step = 4
						trigger = False
						phone_fast.delete()

			if step == 4:			
				form = AppointmentForm(request.POST, request.FILES)
				if form.is_valid() and trigger:
					description = None
					if form.cleaned_data['appointment_form_text'] or form.cleaned_data['appointment_form_img']:
						description = Appointment_Description()
						description.appointment_description_text = form.cleaned_data['appointment_form_text']
						description.appointment_description_img = form.cleaned_data['appointment_form_img']
						description.save()
					for x in dates:
						appointment = Appointment()
						appointment.appointment_expert = expert
						appointment.appointment_client = request.user.client
						appointment.appointment_type_work = type_work
						if description:
							appointment.appointment_description = description
						appointment.appointment_time = x['time']
						appointment.set_date(x['date'])
						appointment.save()
					return redirect(getUrl('appointment_list',add="appointment"))
						
			return render_to_response('tableappointmentnotuser.html',
										{
											'user': request.user,
											'notes':notes,
											'title': title,
											'expert':expert,
											'text':text,
											'form':form,
											'dates':dates,
											'type_work':type_work,
											'step':step,
											'phone_fast':phone_fast,
											'csrf_token': GetFormCSRF(request)
										})
		except Expert.DoesNotExist:
			raise Http404("Специалист не найден")
		except Type_Work.DoesNotExist:
			raise Http404("Тип приема не найден")