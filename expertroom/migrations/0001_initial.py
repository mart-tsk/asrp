# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import django.contrib.auth.models


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
        ('timetable', '0003_auto_20151024_1048'),
    ]

    operations = [
        migrations.CreateModel(
            name='Expert',
            fields=[
                ('user_ptr', models.OneToOneField(serialize=False, parent_link=True, auto_created=True, primary_key=True, to=settings.AUTH_USER_MODEL)),
                ('expert_patronymic', models.CharField(default='', max_length=30)),
                ('expert_description', models.CharField(default='', max_length=2000)),
                ('expert_image', models.ImageField(upload_to='expert', default=None)),
                ('expert_filial', models.ForeignKey(to='timetable.Filial')),
                ('expert_speciality', models.ForeignKey(to='timetable.Speciality')),
            ],
            options={
                'verbose_name_plural': 'Специалисты',
                'verbose_name': 'Специалист',
            },
            bases=('auth.user',),
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
    ]
