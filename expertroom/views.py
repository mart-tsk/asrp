from django.shortcuts import render_to_response, redirect, HttpResponse
from timetable.models import Appointment_Description, Appointment, Client, Appointment_Answer
from expertroom.models import Expert
from django.core.urlresolvers import reverse
from django.views.generic import View
from tools.csrf import GetFormCSRF
from tools.decorators import expert_auth
from django.utils.decorators import method_decorator
from django.http import Http404
from expertroom.form import AppointmentResponseForm
from tools.notification import Notification

class AppointmentListExpert(View):
	@method_decorator(expert_auth)
	def dispatch(self, *args, **kwargs):
		return super(AppointmentListExpert, self).dispatch(*args, **kwargs)

	def get(self,request):
		notes = []
		title = "Список заявок"
		appointment_list = Appointment.objects.filter(appointment_expert=request.user)
		return render_to_response('expert/appointment_list.html',
										{
											'user': request.user,
											'title': title,
											'appointment_list':appointment_list,
											'notes':notes,  
										})

class AppointmentExpert(View):
	@method_decorator(expert_auth)
	def dispatch(self, *args, **kwargs):
		return super(AppointmentExpert, self).dispatch(*args, **kwargs)

	def get(self,request,appointment_id):
		try:
			csrf_token = GetFormCSRF(request)
			notes = []
			title = "Заявка №" + appointment_id
			appointment = Appointment.objects.get(id=appointment_id)
			if appointment.appointment_answer:
				data = {
					'appointment_responce_form_text': appointment.appointment_answer.appointment_desc,
					'appointment_responce_form_img': appointment.appointment_answer.appointment_ansver_img
				}
				form = AppointmentResponseForm(data)
			else:
				form = AppointmentResponseForm()
			return render_to_response('expert/appointmentresponse.html',
										{
											'user': request.user,
											'title': title,
											'appointment':appointment,
											'notes':notes,
											'form': form,
											'csrf_token':csrf_token,  
										})

		except Appointment.DoesNotExist:
			raise Http404("Заявка не найдена")

	def post(self,request, appointment_id):
		try:				
			form = AppointmentResponseForm(request.POST, request.FILES)
			appointment = Appointment.objects.get(id=appointment_id)
			notes = []
			title = "Ответ на заявку"
			csrf_token = GetFormCSRF(request)
			if form.is_valid():
				response = Appointment_Answer()
				response.appointment_desc = form.cleaned_data['appointment_responce_form_text']
				if form.cleaned_data['appointment_responce_form_img']:
					response.appointment_ansver_img = form.cleaned_data['appointment_responce_form_img']
				response.save()
				appointment.appointment_answer = response
				appointment.close()
				appointment.save()
				note = Notification(Notification.enumTypeNotification.Succes,"Ответ на заявку отправлен", "")
				notes.append(note)
				return render_to_response('expert/appointmentresponseview.html',
										{
											'user': request.user,
											'title': title,
											'appointment':appointment,
											'notes':notes,
											'response': response,
											'csrf_token':csrf_token,  
										})
		except appointment.DoesNotExist:
			raise Http404("Appointment not found")