from django.contrib import admin
from expertroom.models import Expert

# Register your models here.


class ExpertAdmin(admin.ModelAdmin):
	list_display = ['username', 'expert_speciality_name', 'first_name', 'last_name', 'expert_organization', 'expert_filial']

admin.site.register(Expert, ExpertAdmin)