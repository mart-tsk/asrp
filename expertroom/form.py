from django import forms
from django.forms import widgets
from timetable.models import Speciality, Client, City
from expertroom.models import Expert

class AppointmentResponseForm(forms.Form):
	appointment_responce_form_text = forms.CharField(label='Ответ', widget=forms.Textarea(attrs={'class': 'form-control noresize'}), required=False)
	appointment_responce_form_img = forms.ImageField(label='Изображение', required=False)
