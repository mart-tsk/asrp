from django.conf.urls import include, url
from django.views.generic import TemplateView
from expertroom.views import AppointmentListExpert, AppointmentExpert

urlpatterns = [
	url(r'^appointments/$', AppointmentListExpert.as_view(), name='expert_appointment_list'),
	url(r'^appointment/(?P<appointment_id>.[^\/]*)$', AppointmentExpert.as_view(), name='expert_appointment'),
]