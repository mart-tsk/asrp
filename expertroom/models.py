from django.db import models
from django.contrib.auth.models import User, UserManager
# Create your models here.

class Expert(User):	
	expert_filial = models.ForeignKey('timetable.Filial')
	expert_speciality = models.ForeignKey('timetable.Speciality')
	expert_patronymic = models.CharField(max_length=30, default="")
	expert_description = models.CharField(max_length=2000, default="")
	expert_image = models.ImageField(upload_to='expert',default=None)
	objects = UserManager()

	@property
	def full_name(self):
		if not hasattr(self, '_full_name'):
			self._full_name = self.last_name + " " + self.first_name + " " + self.client_patronymic
		return self._full_name

	def expert_organization(self):
		return self.expert_filial.filial_organization.organization_name

	def expert_filial_name(self):
		return self.expert_filial.filial_name
		
	def expert_speciality_name(self):
		return self.expert_speciality.speciality_name

	def expert_full_name(self):
		return self.first_name + " " + self.last_name + " " + self.expert_patronymic

	class Meta:
		verbose_name = 'Специалист'
		verbose_name_plural = 'Специалисты'