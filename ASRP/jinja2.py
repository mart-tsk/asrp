from django.contrib.staticfiles.storage import staticfiles_storage
from django.core.urlresolvers import reverse
from jinja2 import Environment
from tools.url import getUrl, hiddenPost
from tools.auth_be import getUserType

def environment(**options):
    env = Environment(**options)
    env.globals.update({
        'static': staticfiles_storage.url,
        'url': reverse,
        'urlGet': getUrl,
        'type':type,
        'getUserType':getUserType,
        'hiddenPost':hiddenPost
    })
    return env