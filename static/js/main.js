jQuery(document).ready(function() {
	var TimeSummary = '';
	var CheckedRange = 0;
	$('#telnum').bind("change keyup input click", function() {
		if (this.value.match(/[^0-9]/g)) {
			this.value = this.value.replace(/[^0-9]/g, '');
		}
	});

	$(".chb").click(function() {
		var key = $(this).attr("time") + "|" + $(this).attr("date") + "|" + $(this).attr("table_type");
		var row = +this.id.split("_")[1].split("/")[0];
		var rowscount = this.id.split("_")[1].split("/")[1];
					rowscount = +rowscount + row -1;
		var cell = +this.id.split("_")[2].split("/")[0];
		var cellscount = +(this.id.split("_")[2].split("/")[1]);
					cellscount = +cellscount + cell - 1;
		var date = this.id.split("_")[3]
		
		// console.log("Row: " + row + "/" + rowscount + " Cell: " + cell + "/" + cellscount)
		// console.log(key)
		if (this.checked){
			CheckedRange++;
			var count = 0;
			for (var i = 1; i <= rowscount; i++){
				for (j=1; j<= cellscount; j++){
					if((i==row)&&(j==cell)){
						count = 1;
					}
					else{
						if (count>0){
							count--;
							e = document.getElementById("cell_"+i+"/"+ (rowscount-i+1) +"_"+j+"/"+ (cellscount-j+1)+"_"+date);
							if($(e).attr("close")){

							}else{
								e.disabled = false;
							}
						}
						else{
							document.getElementById("cell_"+i+"/"+ (rowscount-i+1) +"_"+j+"/"+ (cellscount-j+1)+"_"+date).disabled = true;
						}

					}
				}
			}
			TimeSummary += key +'/';
		}
		else{
			CheckedRange--;
			if (CheckedRange==0){
				for (var i = 1; i <= rowscount; i++){
					for (j=1; j<= cellscount; j++){
						e = document.getElementById("cell_"+i+"/"+ (rowscount-i+1) +"_"+j+"/"+ (cellscount-j+1)+"_"+date);
						if(!$(e).attr("close")){
							document.getElementById("cell_"+i+"/"+ (rowscount-i+1) +"_"+j+"/"+ (cellscount-j+1)+"_"+date).disabled = false;
						}
					}
				}
			}
			else{
				var count = 0;
				var elem;
				for (var i = 1; i <= rowscount; i++){
					for (j=1; j<= cellscount; j++){
						if((i==row)&&(j==cell)){
							elem.disabled = false;
						}
						else{
							document.getElementById("cell_"+i+"/"+ (rowscount-i+1) +"_"+j+"/"+ (cellscount-j+1)+"_"+date).disabled = true;

						}
						elem = document.getElementById("cell_"+i+"/"+ (rowscount-i+1) +"_"+j+"/"+ (cellscount-j+1)+"_"+date);
					}
				}
			}
			TimeSummary = TimeSummary.replace(key+'/','');
		}
	});
	

	$('#submitbtn').click(function(){
		$.redirect(this.form.action,
				{
					csrfmiddlewaretoken:document.getElementById('csrfmiddlewaretoken').value, 
					text: TimeSummary.substring(0, TimeSummary.length - 1),
					typeQuery: "zp",
					expert_id:document.getElementById('expert_id').value,
				}); 
    });
	$('#select_sty').change(function(){
		var speciality_id = $("#select_sty").val()
		$('#select_exp').disabled=true;
		$.post(
			"/operator/appointments/ajax/", 
			{ 
				speciality_id: speciality_id,
				csrfmiddlewaretoken:document.getElementById('csrfmiddlewaretoken').value 
			},
  			function(data){
  				$("#select_exp").empty();
  				var json = '{ "expert":'+data+'}'
  				var expert_list = JSON.parse(json).expert
  				
  				for (var exp in expert_list){
  					$("#select_exp").append( $('<option value="'+ expert_list[exp].id + '">'+ expert_list[exp].name + '</option>'));
  				}
    			$('#select_exp').disabled=true;
  			}
  		);

	});
    $('#sendExpert').click(function(){
    	var app_id =  $('#appointment_id').val()
    	var expert_id = $("#select_exp").val()
       	$.post(
			"/operator/appointments/ajax/", 
			{ 
				app_id: app_id,
				expert_id: expert_id,
				csrfmiddlewaretoken:document.getElementById('csrfmiddlewaretoken').value 
			},
  			function(data){
  				var expert = JSON.parse(data);
  				$("#exp_name").text("Специалист: " + expert.exp);
  				$("#exp_spec").text("Специальность: " + expert.spec);
  			}
  		);
    });
    $('#sendTime').click(function(){
    	var app_id =  $('#appointment_id').val();
    	var form = this.form;
    	var str = $(form).serialize()
       	$.post(
			"/operator/appointments/ajax/", 
			{ 
				app_id: app_id,
				date_field_day: $('#date_field_day').val(),
				date_field_month: $('#date_field_month').val(),
				date_field_year: $('#date_field_year').val(),
				time_field: $('#time_field').val(),
				csrfmiddlewaretoken:document.getElementById('csrfmiddlewaretoken').value 
			},
  			function(data){
  				if (data=="nonvaliddateortime"){
  					$("#time_dng").text("Дата или время введены неверно")
  					$("#time_suc").text("")
  				}
  				else{
	  				var date_time = JSON.parse(data);
	  				$("#app_date").text("Дата: " + date_time.change_date_time_form_date);
	  				$("#app_time").text("Время: " + date_time.change_date_time_form_time);
	  				$("#time_dng").text("")
  					$("#time_suc").text("Дата и время успешно изменены")
	  			}
  			}
  		);
    });
    
});
