import datetime
import calendar
import re

def getDay(date=None):
	if not date:
		date = datetime.datetime.strftime(datetime.datetime.now(), "%Y-%m-%d").split("-")		
	else:
		date = date.split("-")		
	return Days[calendar.weekday(int(date[0]),int(date[1]),int(date[2]))+1]

def curDate(offset=0):			
	return (datetime.date.today() + datetime.timedelta(days=offset)).strftime("%Y-%m-%d")

Days = {
	1:'Monday',
	2:'Tuesday',
	3:'Wednesday',
	4:'Thursday',
	5:'Friday',
	6:'Saturday',
	7:'Sunday'
}

def checkDate(cdate=None):
		d = datetime.datetime.strptime(cdate, "%Y-%m-%d").date()
		today = datetime.date.today()
		if d < today :
			return False
		return True	

def checkTimeString(ctime=None):
		if re.match("[\d]{1,2}:[\d]{2}",ctime):
			if int(ctime.split(":")[0]) <= 24 and int(ctime.split(":")[1])<60:
				return True
		return False
