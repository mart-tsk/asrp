from django.core.urlresolvers import reverse

def getUrl(name,**kwargs):
	url = reverse(name) + "?"
	trigger = False
	for key, value in kwargs.items():
		if key=='args':
			for k,v in value.items():				
				cache = str(k) + "=" + str(v)
				if trigger:			
					url += "&"
				else:
					trigger = True
				url += cache
		else:
			cache = str(key) + "=" + str(value)
			if trigger:			
				url += "&"
			else:
				trigger = True
			url += cache
	return url

def hiddenPost(key, value):	
	return '<input type="hidden" name="' + str(key) + '" value="'+ str(value) + '">'