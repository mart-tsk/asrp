import string
import random

def GenerateString(count):
	return ''.join(random.choice(string.ascii_uppercase) for i in range(count))