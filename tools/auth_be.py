from django.conf import settings
from django.contrib.auth.backends import ModelBackend
from django.core.exceptions import ImproperlyConfigured
from django.db.models import get_model
from timetable.models import Client
from expertroom.models import Expert
from operatorroom.models import Operator

class CustomUserModelBackend(ModelBackend):
    def authenticate(self, username=None, password=None):
        for x in self.user_class: 
            try:
                user = x.objects.get(username=username)
                if user.check_password(password):
                    return user
            except x.DoesNotExist:
                pass

    def get_user(self, user_id):
        for x in self.user_class:
            try:
                return x.objects.get(pk=user_id)
            except x.DoesNotExist:
                pass

    @property
    def user_class(self):
        if not hasattr(self, '_user_class'):
            self._user_class = []
            for x in settings.CUSTOM_USER_MODEL:
                self._user_class.append(get_model(str(x)))
            if not self._user_class:
                raise ImproperlyConfigured('Could not get custom user model')
        return self._user_class

def getUserType(user):
    if isinstance(user,Client):
        return "Client"
    elif isinstance(user,Operator):
        return "Operator"
    elif isinstance(user,Expert):
        return "Expert"
    else:
        return 0