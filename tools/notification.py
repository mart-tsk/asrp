from enum import Enum

def enum(**enums):
    return type('Enum', (), enums)

class Notification(object):

	enumTypeNotification = enum(Warning=0, Danger=1, Succes=3, Info=4)

	def __init__(self, typeNot, title, text):
		super(Notification, self).__init__()
		self.typeNotification = typeNot
		self.title = title
		self.text = text

"""
	Example
	note = Notification(Notification.enumTypeNotification.Warning,"Неверный телефон", "fdg")
				return render_to_response('test.html', {
														'notes':[note]
														})
"""