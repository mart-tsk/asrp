from django.template.context_processors import csrf

def GetFormCSRF(request):
	c = csrf(request)
	c = c['csrf_token']
	return '<input type="hidden" id="csrfmiddlewaretoken" name="csrfmiddlewaretoken" value="' + str(c) + '">'