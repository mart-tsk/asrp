from tools.auth_be import getUserType
from django.shortcuts import redirect, HttpResponse
from django.core.urlresolvers import reverse

def client_auth(a_view):
	def _wrapped_view(request, *args, **kwargs):
		if request.user.is_authenticated() and getUserType(request.user) == "Client":
			return a_view(request, *args, **kwargs)
		return redirect(reverse('timetable_root'))
	return _wrapped_view

def client_auth_conf(a_view):
	def _wrapped_view(request, *args, **kwargs):
		if request.user.is_authenticated() and getUserType(request.user) == "Client" and request.user.is_confirmed():
			return a_view(request, *args, **kwargs)
		return redirect(reverse('timetable_root'))
	return _wrapped_view

def operator_auth(a_view):
	def _wrapped_view(request, *args, **kwargs):
		if request.user.is_authenticated() and getUserType(request.user) == "Operator":
			return a_view(request, *args, **kwargs)
		return redirect(reverse('timetable_root'))
	return _wrapped_view	

def expert_auth(a_view):
	def _wrapped_view(request, *args, **kwargs):
		if request.user.is_authenticated() and getUserType(request.user) == "Expert":
			return a_view(request, *args, **kwargs)
		return redirect(reverse('timetable_root'))
	return _wrapped_view	