from django import forms
from django.forms import widgets
from django.forms.extras.widgets import SelectDateWidget
from timetable.models import Speciality, Client, City
from expertroom.models import Expert

class ExpertModelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.expert_full_name()

class ChangeExpertForm(forms.Form):
	change_expert_form_speciality = forms.ModelChoiceField(label='Специальность',widget=widgets.Select(attrs={'class':'form-control','id':'select_sty',}),queryset=Speciality.objects.all(),empty_label=None)
	change_expert_form_expert = ExpertModelChoiceField(label='Специалист',widget=widgets.Select(attrs={'class':'form-control','id':'select_exp'}),queryset=Expert.objects.all(),empty_label=None)

	def __init__(self, *args, **kwargs):
		filial_id = kwargs.pop('filial_id')
		specialty = kwargs.pop('specialty')
		expert = kwargs.pop('expert')
		super(ChangeExpertForm, self).__init__(*args, **kwargs)
		v = Expert.objects.filter(expert_filial = filial_id).values_list('expert_speciality',flat=True)
		self.fields['change_expert_form_speciality'].queryset = Speciality.objects.filter(id__in=list(v))
		self.fields['change_expert_form_speciality'].initial = specialty
		self.fields['change_expert_form_expert'].initial = expert
		self.fields['change_expert_form_expert'].queryset = Expert.objects.filter(expert_filial = filial_id,expert_speciality = specialty)

class ChangeDateTimeForm(forms.Form):
	change_date_time_form_date = forms.DateField(label='Дата',widget=SelectDateWidget(attrs={'class':'form-control','id':'date_field'}))
	change_date_time_form_time = forms.CharField(label='Время',widget=widgets.TextInput(attrs={'class':'form-control','id':'time_field'}))