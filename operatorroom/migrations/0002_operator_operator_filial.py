# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('operatorroom', '0001_initial'),
        ('timetable', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='operator',
            name='operator_filial',
            field=models.ForeignKey(to='timetable.Filial'),
        ),
    ]
