from django.shortcuts import render_to_response, redirect, HttpResponse
from timetable.models import Appointment_Description, Appointment, Client
from expertroom.models import Expert
from django.core.urlresolvers import reverse
from django.views.generic import View
from operatorroom.models import Operator
from tools.notification import Notification
from tools.csrf import GetFormCSRF
from operatorroom.form import ChangeExpertForm, ChangeDateTimeForm
from django.http import Http404
import json
from tools.day import checkDate, checkTimeString
from tools.decorators import operator_auth
from django.utils.decorators import method_decorator

class AppointmentListViewOR(View):
	@method_decorator(operator_auth)
	def dispatch(self, *args, **kwargs):
		return super(AppointmentListViewOR, self).dispatch(*args, **kwargs)

	def get(self,request):
		if not request.user.is_authenticated():
			return redirect(reverse('timetable_login'))
		notes = []
		title = "Список заявок"
		filial_id = request.user.operator_filial.id;
		
		try:
			appointment = Appointment.objects.get(id=request.GET.get('id'))		
			if request.GET.get('status') == 'CF':
				appointment.confirm()
				note = Notification(Notification.enumTypeNotification.Info,"Заявка подтверждена", "Статус заявки <br>" + appointment.appointment_client.full_name + "<br> K:" + appointment.appointment_expert.expert_full_name() + "<br> Ha: " + appointment.appointment_time + "<br> изменен на \"Потверждено\"")
				notes.append(note)
			elif request.GET.get('status') == 'CL':
				appointment.close()
				note = Notification(Notification.enumTypeNotification.Info,"Заявка закрыта", "Статус заявки <br>" + appointment.appointment_client.full_name + "<br> K:" + appointment.appointment_expert.expert_full_name() + "<br> Ha: " + appointment.appointment_time + "<br> изменен на \"Закрыта\"")
				notes.append(note)
			else:
				pass
			appointment.save()
		except:
			pass
		expertlist = Expert.objects.filter(expert_filial = request.user.operator_filial).values_list('id',flat=True)		
		appointment_list = Appointment.objects.filter(appointment_expert__in = list(expertlist))
		return render_to_response('appointment_list _OR.html',
										{
											'user': request.user,
											'title': title,
											'appointment_list':appointment_list,
											'notes':notes,  
										})
		

class AppointmentView(View):
	@method_decorator(operator_auth)
	def dispatch(self, *args, **kwargs):
		return super(AppointmentView, self).dispatch(*args, **kwargs)
	def get(self, request, appointment_id):
		notes = []
		title = "Просмотр заявки"
		csrf_token = GetFormCSRF(request)
		appointment = Appointment.objects.get(id=appointment_id)
		filial_id = request.user.operator_filial.id;
		expert_form = ChangeExpertForm(filial_id=filial_id,specialty=appointment.appointment_expert.expert_speciality,expert=appointment.appointment_expert)
		date_time_form = ChangeDateTimeForm()
		return render_to_response('appointment_OR.html',
										{
											'user': request.user,
											'title': title,
											'appointment':appointment,
											'notes':notes,
											'expert_form': expert_form,
											'date_time_form':date_time_form,
											'csrf_token': csrf_token ,
										})
class Ajax(View):
	def get(self,request):
		return HttpResponse("FCKU")
	def post(self, request):

		if request.POST.get('speciality_id'):
			speciality_id = request.POST.get('speciality_id')
			experts = []
			qs = Expert.objects.filter(expert_speciality=speciality_id)
			for expert in qs:
				experts.append({"id":expert.id,"name":expert.expert_full_name()})
			return HttpResponse(json.dumps(experts))
		if request.POST.get('expert_id') and request.POST.get('app_id'):
			app_id = request.POST.get('app_id')
			expert_id = request.POST.get('expert_id')
			e = Expert.objects.get(id=expert_id)
			a = Appointment.objects.get(id=app_id)
			a.appointment_expert = e
			a.save()
			return HttpResponse(json.dumps({"exp":a.appointment_expert.expert_full_name(),"spec":a.appointment_expert.expert_speciality.speciality_name}))
		if request.POST.get('app_id') and request.POST.get('time_field') and request.POST.get('date_field_day') and request.POST.get('date_field_month') and request.POST.get('date_field_year'):
			date_field_day = request.POST.get('date_field_day')
			date_field_month = request.POST.get('date_field_month')
			date_field_year = request.POST.get('date_field_year')
			time_field = request.POST.get('time_field')
			app_id = request.POST.get('app_id')
			a = Appointment.objects.get(id=app_id)
			data = {
					"change_date_time_form_date": date_field_year  + "-"+date_field_month + "-" +date_field_day,
					"change_date_time_form_time": time_field
					}
			if checkDate(data['change_date_time_form_date']) and checkTimeString(data['change_date_time_form_time']):
				form = ChangeDateTimeForm(data)
				if form.is_valid():
					a.appointment_time = data['change_date_time_form_time']
					a.appointment_date = data['change_date_time_form_date']
					a.save()
					return HttpResponse(json.dumps(data))
		return HttpResponse("nonvaliddateortime")

