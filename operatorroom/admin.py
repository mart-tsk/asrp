from django.contrib import admin
from django.db import models
from .models import Operator
from django import forms
from timetable.admin import getter_for_related_field, RelatedFieldAdminMetaclass, RelatedFieldAdmin


# Register your models here.

class OperatorAdmin(RelatedFieldAdmin):
	list_display = ['username', 'organization', 'operator_filial', 'first_name', 'last_name']
	def queryset(self, request):
		qs = super(OperatorAdmin, self).queryset(request)
		if request.user.profile.operator_filial: # If the user has a location
			# change the queryset for this modeladmin
			qs = qs.filter(location=request.user.profile.operator_filial)
		return qs

admin.site.register(Operator,OperatorAdmin)