from django.db import models
from django.contrib.auth.models import User, UserManager
from timetable.models import Filial

class Operator(User):
	operator_filial = models.ForeignKey('timetable.Filial')
	
	objects = UserManager()
	def organization(self):
		return self.operator_filial.filial_organization.organization_name
	class Meta:
		verbose_name = 'Оператор'
		verbose_name_plural = 'Операторы'