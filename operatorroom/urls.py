from django.conf.urls import include, url
from operatorroom.views import AppointmentListViewOR, AppointmentView, Ajax
from django.views.generic import TemplateView

urlpatterns = [
	 url(r'^appointments/$', AppointmentListViewOR.as_view(), name='operatorroom_AppointmentListView'),
	 url(r'^appointments/detail/(?P<appointment_id>.[^\/]*)$', AppointmentView.as_view(), name='operatorroom_AppointmentDetail'),
	 url(r'^appointments/ajax/$', Ajax.as_view(), name='ajax'),
]
	